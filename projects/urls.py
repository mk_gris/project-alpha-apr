from django.urls import path
from projects.views import list_project, show_project_detail, create_project


urlpatterns = [
    path("", list_project, name="list_projects"),
    path("<int:id>/", show_project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
