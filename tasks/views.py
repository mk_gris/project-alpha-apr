from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def list_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "list_task": tasks,
    }
    return render(request, "tasks/list_task.html", context)
